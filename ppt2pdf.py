__author__ = 'Arthur'
#coding:utf-8
import win32com.client
import os

#---------------------------------------#
# 以默认格式另存为指定位置 #
#---------------------------------------#

#~ # Open PowerPoint
Application = win32com.client.Dispatch("PowerPoint.Application")
Application.Visible = False

#~ # Create new presentation
myPresentation = Application.Presentations.Add()

Slide = myPresentation.Slides.Add(1, 12)

#~ myPresentation.SaveAs('abc')                              # error: 没有路径
#~ myPresentation.SaveAs('abc.pptx')                         # error: 没有路径
#~ myPresentation.SaveAs('D:\\test dir\\hello world.pptx')   # error: 不存在的路径，不可以
myPresentation.SaveAs('D:\\abc1.pdf')                     # error: 直接保存为pdf格式出错

# myPresentation.SaveAs('D:\\abc1')                            # Right: 完整路径，以默认格式保存
#~ myPresentation.SaveAs('D:\\abc1.pptx')                    # Right: 完整路径,指明保存格式
#~ myPresentation.SaveAs('D:\\abc1.ppt')                     # Right: 完整路径,指明保存格式
#~ myPresentation.SaveAs('D:\\hello world.pptx')             # Right: 文件名中有空格可以
__author__ = 'Arthur'
import win32com.client
from win32com.client import Dispatch,constants
import win32com.client as win32
import os
from pyPdf import PdfFileReader

# def getWordPage(filename):
#     try:
#         w = win32.gencache.EnsureDispatch('Word.Application')
#         w.Visible = True
#         w.Activate()
#         doc = w.Documents.Open(filename)
#         w.ActiveDocument.Repaginate()
#         page =  w.ActiveDocument.BuiltInDocumentProperties(constants.wdPropertyPages)
#         return page
#     except Exception,e:
#         return e
#     finally:
#         w.Quit(constants.wdDoNotSaveChanges)

def getPPTPage(filename):
    try:
        p = win32com.client.Dispatch("PowerPoint.Application")
        ppt = p.Presentations.Open(filename)
        slide_count = len(ppt.Slides)
        return slide_count
    except Exception,e:
        return e
    finally:
        p.Quit()

def getWordPage(filename):
  w = Dispatch("Word.Application")
  arr = filename.split('.')
  arr[-1]='pdf'
  output = '.'.join(arr)
  try:
    doc = w.Documents.Open(filename, ReadOnly = 1)
    doc.ExportAsFixedFormat(output, constants.wdExportFormatPDF, 
        Item = constants.wdExportDocumentWithMarkup, CreateBookmarks = constants.wdExportCreateHeadingBookmarks)
    pdf = PdfFileReader(file(output,"rb"))
    pages = pdf.getNumPages()
    return pages
  except Exception:
    return -1
  finally:
    w.Quit(constants.wdDoNotSaveChanges)

if __name__=='__main__':
    pptPath = "C:\Users\Administrator\Documents\\testppt.pptx"
    wordPath = "C:\Users\Arthur\Documents\wordtest.docx"
    try:
        #print getPPTPage(pptPath)
        #print getWordPage(wordPath)
        print getWordPage(wordPath)
                      
    except Exception,e:
        print e

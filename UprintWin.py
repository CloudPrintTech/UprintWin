#encoding=utf8
from flask import Flask,request,render_template,redirect,url_for,send_from_directory,jsonify,abort
from werkzeug.utils import secure_filename
from oss.oss_api import *
import os
import pymysql
import pymysql.cursors
import redis
import file_page
import win32com.client
from win32com.client import Dispatch,constants
import pythoncom
import win32com.client as win32
import os
from pyPdf import PdfFileReader

UPLOAD_FOLDER = 'static'+os.sep+'Uploads'
ALLOWED_EXTENSIONS = set(['docx', 'doc', 'ppt', 'pptx', 'pdf'])
app = Flask(__name__)
app.config['UPLOAD_DIR'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024

@app.route('/')
def index():
    return redirect(url_for('upload'),302)


@app.route('/upload',methods=["GET","POST"])
def upload():
    if request.method=='GET':
        return render_template('upload.html')
    elif request.method == 'POST':
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            if os.path.exists(app.config['UPLOAD_DIR']):
                os.mkdir(app.config['UPLOAD_DIR'])
            file.save(os.path.join(app.config['UPLOAD_DIR'], filename))
            return redirect(url_for('uploaded_file',
                                    filename=filename))
def getWordPage(filename):
    pythoncom.CoInitialize()
    w = Dispatch("Word.Application")
    arr = filename.split('.')
    arr[-1]='pdf'
    output = '.'.join(arr)
    try:
        doc = w.Documents.Open(filename, ReadOnly = 1)
        doc.ExportAsFixedFormat(output, constants.wdExportFormatPDF,
            Item = constants.wdExportDocumentWithMarkup, CreateBookmarks = constants.wdExportCreateHeadingBookmarks)
        pdf = PdfFileReader(file(output,"rb"))
        pages = pdf.getNumPages()
        return pages
    except Exception:
        return -1
    finally:
        w.Quit(constants.wdDoNotSaveChanges)

@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],
                               filename)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@app.route('/v1/file/pages',methods=["GET"])
def get_doc_page():
    file_key = request.args.get('file_key')
    file_type = request.args.get('file_type')
    endpoint="oss-cn-shanghai.aliyuncs.com"
    accessKeyId, accessKeySecret="vKEirKqFefWcseJ7","a9Zeg5f39598ASF8cGjNl0oOyWjdjD"
    oss = OssAPI(endpoint, accessKeyId, accessKeySecret)
    dir_path = os.path.join(os.path.dirname(__file__),'student-documents')
    if not os.path.exists(dir_path):
        os.mkdir(dir_path)
    file_path = dir_path + '/'+file_key+'.'+file_type
    output = oss.get_object_to_file("student-documents",file_key,file_path)

    page,doc = getWordPage1(file_path)
    doc.Close()
    return jsonify({'page': page})


@app.route('/v2/file/pages',methods=["GET"])
def get_file_page():
    param = request.get_json(force=True)
    token,task_id = param['token'],param['task_id']
    redisconn = redis.Redis('api.in.uprintf.com',6379)
    student_id = redisconn.get(token)
    if not student_id:
        abort(401)
    dbconn = pymysql.connect(host='api.in.uprintf.com',
                             user='test',
                             password='Playstation2',
                             db='cloudprinter',
                             charset='utf8',
                             cursorclass=pymysql.cursors.DictCursor)
    try:
        with dbconn.cursor() as cursor:
            sql = "select file_key,file_name,pages from print_task WHERE task_id=%d and student_id=%d"
            cursor.execute(sql,(task_id,student_id))
            result = cursor.fetchone()
            if not result:
                return err("resource not exist")
            if result['pages']:
                return success([{'pages':result['pages']}])
         #从oss上下载文件
        endpoint="oss-cn-shanghai-internal.aliyuncs.com"
        accessKeyId, accessKeySecret="vKEirKqFefWcseJ7","a9Zeg5f39598ASF8cGjNl0oOyWjdjD"
        oss = OssAPI(endpoint, accessKeyId, accessKeySecret)
        root_path = os.path.join(os.path.dirname(__file__),'student-documents')
        dir_path = os.path.join(root_path,str(student_id)+str(int(time.time())))
        if not os.path.exists(dir_path):
            os.mkdir(dir_path)
        file_path = dir_path + '/'+result['file_name']
        oss.get_object_to_file("student-documents",result['file_key'],file_path)
        ext = result['file_name'].split('.')[-1]
        if ext=='doc' or ext=='docx':
            page = file_page.getWordPage(file_path)
        elif ext=='ppt' or ext=='pptx':
            page = file_page.getPPTPage(file_path)
        if page != -1:
            sql = "update print_task set pages=%d WHERE task_id=%d"
            cursor.execute(sql,(page,task_id))
            dbconn.commit()
            return success([{'pages':page}])
        else:
            return err()
    finally:
        dbconn.close()

def err(msg='fail'):
    return jsonify({'error':-1,"results":[],'msg':msg})

def success(results=[],msg="success"):
    return jsonify({'error':-1,"results":results,'msg':msg})

def getWordPage1(filename):
    try:
        w = win32.gencache.EnsureDispatch('Word.Application')
        w.Visible = True
        w.Activate()
        doc = w.Documents.Open(filename)
        w.ActiveDocument.Repaginate()
        page = w.ActiveDocument.BuiltInDocumentProperties(constants.wdPropertyPages)
        return page,doc
    except Exception:
        return -1,doc
    # finally:
    #     w.Quit(constants.wdDoNotSaveChanges)

if __name__ == '__main__':
    app.run(host='win.in.uprintf.com',debug=True,port=80)
    # app.run(debug=True,port=80)
